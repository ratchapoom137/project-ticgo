import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import { Button, Icon } from '@ant-design/react-native'

export default class Logo extends Component {
    kuy = () => {
        console.log('ssss')
    }
    render() {
        return (
            <View style={styles.container}>
                <Button style={styles.facebookButton} onPress={() => {this.kuy()}} type="primary"><Icon name="facebook" size="sm" color="white" /> Login with Facebook</Button>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        alignItems: 'center'
    },
    facebookButton: {
        width: 300,
        marginVertical: 10,
        paddingHorizontal: 12,
        height: 38
    },
});