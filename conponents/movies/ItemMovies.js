import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar } from 'react-native';
import { Carousel, Icon, Card, Grid, ListView } from '@ant-design/react-native'
import axios from 'axios';

class ItemsPage extends Component {

    state = {
        collection: []
    }

    componentDidCatch() {
        this.getMovies()
    }

    getMovies = () => {
        axios
            .get('https://zenon.onthewifi.com/ticGo/movies')
            .then(res => {
                this.setState({ collection: res.data })
            })
            .catch(e => { console.log(e) })
            .finally(() => { console.log('Finally') })
    }
    goToItem = state => {
        const { gotoItem } = this.props
        gotoItem(state)
    }

    renderItem = ({ item, index }) => {
        return (
            <View style={styles.container}>
                <View
                    style={{
                        backgroundColor: 'white',
                        height: 50,
                        alignItems: 'center',
                        justifyContent: 'center',
                    }}
                >
                    <Image style={{ width: 90, height: 90 }}
                        source={require('../images/ticgo.png')} />
                </View>
            </View>
        )
    }
}

export default ItemsPage