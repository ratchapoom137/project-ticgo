import React, { Component } from 'react';
import { StyleSheet, Text, View, TextInput, TouchableOpacity, ScrollView, Button } from 'react-native';
import { WhiteSpace, WingBlank, List, InputItem, Icon } from '@ant-design/react-native';



export default class Logo extends Component {
    state = {
        visible: false,
        email: '',
        password: '',
        firstname: '',
        lastname: '',
    };
    render() {
        return (
            <View style={styles.container}>
            <Text style={styles.textTitle}>Register</Text>
                <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Email"
                    placeholderTextColor="#ffffff"
                />
                <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Password"
                    secureTextEntry={true}
                    placeholderTextColor="#ffffff"
                />
                {/* <InputItem
                        style={styles.inputBox}
                        clear
                        underlineColorAndroid='rgba(0,0,0,0)'
                        placeholderTextColor="#ffffff"
                        value={this.state.email}
                        onChange={email => {
                            this.setState({
                                email,
                            });
                        }}
                        placeholder="Email"
                    >
                    </InputItem> */}
                <View style={styles.button}>
                    <Button
                        title="Register"
                        color="#34444b"
                    />
                </View>


                <Text style={{ color: 'gray' }}>──────────────────────────────</Text>

            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },

    button: {
        width: 300,
        marginVertical: 10,
        paddingHorizontal: 12
    },

    inputBox: {
        width: 300,
        backgroundColor: 'rgba(255, 255,255,0.1)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    lineStyle: {
        borderWidth: 0.5,
        borderColor: 'black',
        margin: 10,
    },
    textTitle: {
        color: '#ffffff',
        fontSize: 24
    }
});