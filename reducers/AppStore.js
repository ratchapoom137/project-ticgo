import { createMemoryHistory } from 'history'
import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
// import { persistStore, persistReducer } from 'redux-persist'
import { routerMiddleware, connectRouter } from 'connected-react-router'
import logger from 'redux-logger'

import LoginReducer from './UserReducer'
// import SaveInfoProduct from './SaveInfoProduct'

// import storage from 'redux-persist/lib/storage'
// const persistConfig = {
//     key: 'root',
//     storage,
// }

const reducers = (history) => combineReducers({
    user: LoginReducer,
    router: connectRouter(history)
})

// const persistReducer = persistReducer(persistConfig, reducers)

export const history = createMemoryHistory()
export const store = createStore(
    reducers(history), // <-- persistReducer
    compose(
        applyMiddleware(
            routerMiddleware(history),
            logger
        )
    )
);

// export const persister = persistStore(store) //ส่งออกไป
// const state = store.getState();
// console.log(state)

// import { createMemoryHistory } from 'history'
// import { createStore, combineReducers, applyMiddleware, compose } from 'redux';
// import { persistStore, persistReducer } from 'redux-persist'
// import { routerMiddleware, connectRouter } from 'connected-react-router'
// import logger from 'redux-logger'

// import LoginReducer from './LoginReducer'
// // import SaveInfoProduct from './SaveInfoProduct'

// import storage from 'redux-persist/lib/storage'
// const persistConfig = {
//     key: 'root',
//     storage,
// }

// const reducers = (history) => combineReducers({
//     user: LoginReducer,
//     router: connectRouter(history)
// })

// const persistReducer = persistReducer(persistConfig, reducers)

// export const history = createMemoryHistory()
// export const store = createStore(
//     reducers(history), // <-- persistReducer
//     compose(
//         applyMiddleware(
//             routerMiddleware(history),
//             logger
//         )
//     )
// );

// // export const persister = persistStore(store) //ส่งออกไป
// // const state = store.getState();
// // console.log(state)