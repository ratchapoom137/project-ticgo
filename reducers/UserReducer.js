export default (state = {}, action) => {
    switch (action.type) {
        case 'ADD_USER':
            return action.user
        case 'EDIT_USER':
            return {
                ...state,
                firstName: action.firstName,
                lastName: action.lastName,
            }
        case 'LOGOUT':
            return state
        default:
            return state
    }
}