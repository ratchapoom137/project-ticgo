import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar } from 'react-native';

export default class Logo extends Component {
    render() {
        return (
            <View style={styles.container}>
                <Image style={{ width: 120, height: 120, borderRadius: 70 }}
                    source={require('./images/ticgo1.jpg')} />
                <Text style={styles.logoText}>TicGo Application</Text>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flexGrow: 1,
        justifyContent: 'flex-end',
        alignItems: 'center'
    },
    logoText: {
        marginVertical: 12,
        fontSize: 18,
        color: 'rgba(255, 255, 255, 0.7)'
    }
});