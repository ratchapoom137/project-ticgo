import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, StatusBar, TouchableOpacity, ScrollView, Animated, ImageBackground } from 'react-native';
import { Carousel, Icon, Card, Grid } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import ListMovies from '../conponents/movies/ListMovies'
import Router from './Router'
import axios from 'axios';

class ImageLoader extends Component {
    state = {
        opacity: new Animated.Value(0),
    }

    onLoad = () => {
        Animated.timing(this.state.opacity, {
            toValue: 1,
            duration: 500,
            useNativeDriver: true,
        }).start();
    }

    render() {
        return (
            <Animated.Image
                onLoad={this.onLoad}
                {...this.props}
                style={[
                    {
                        opacity: this.state.opacity,
                        transform: [
                            {
                                scale: this.state.opacity.interpolate({
                                    inputRange: [0, 1],
                                    outputRange: [0.85, 1],
                                })
                            }
                        ]
                    }, this.props.style,
                ]}
            />
        )
    }
}

class MoviePage extends Component {

    state = {
        selectedTab: 'profile',
        itemMovies: [],
        pathName: '',
        email: '',
    }

    navigateToItemMoviePage = (item) => {
        const { push } = this.props
        push('/itemMovie', {
            item: item
        })
    }

    componentDidMount() {
        axios.get('https://zenon.onthewifi.com/ticGo/movies')
            .then(response => {
                this.setState({
                    itemMovies: response.data
                })
                // console.log(this.state.itemMovies)
            })
            .catch((error) => {
                console.error(error);
            })
            .finally(() => { console.log('Finally') })
    }

    render() {
        // const { user } = this.props
        // console.log("eiei ", this.props)
        return (
            <ScrollView>
                <View style={styles.container}>
                    <View>
                        <View style={{ padding: 5 }}>
                            <Carousel
                                selectedIndex={0}
                                autoplay
                                infinite
                                dots={false}
                            >
                                <ImageBackground style={styles.containerHorizontal}
                                    source={require('../images/advt1.png')} >
                                    <View style={styles.imageSlide} >
                                        <Text style={styles.TextSlide}>Captain Marvel Combo Set</Text>
                                    </View>
                                </ImageBackground>
                                <ImageBackground style={styles.containerHorizontal}
                                    source={require('../images/advt2.png')} >
                                    <View style={styles.imageSlide} >
                                        <Text style={styles.TextSlide}>อลิตา แบทเทิล แองเจิ้ล</Text>
                                    </View>
                                </ImageBackground>
                                <ImageBackground style={styles.containerHorizontal}
                                    source={require('../images/advt3.png')} >
                                    <View style={styles.imageSlide} >
                                        <Text style={styles.TextSlide}>Advance Booking : Captain Marvel</Text>
                                    </View>
                                </ImageBackground>
                                <ImageBackground style={styles.containerHorizontal}
                                    source={require('../images/advt4.png')} >
                                    <View style={styles.imageSlide} >
                                        <Text style={styles.TextSlide}>TG Movie Club Special Gift Card Vaventine</Text>
                                    </View>
                                </ImageBackground>
                                <ImageBackground style={styles.containerHorizontal}
                                    source={require('../images/advt5.png')} >
                                    <View style={styles.imageSlide} >
                                        <Text style={styles.TextSlide}>TG มอบความพิเศษให้แก่สมาชิก TG Movie...</Text>
                                    </View>
                                </ImageBackground>
                            </Carousel>
                        </View>
                    </View>
                    <View style={styles.content}>
                        <Grid
                            data={this.state.itemMovies}
                            columnNum={3}
                            renderItem={(item) => (
                                <TouchableOpacity
                                    onPress={() => this.navigateToItemMoviePage(item)}
                                >
                                    <ImageLoader source={{ uri: item.image }}
                                        style={{ width: '100%', height: 180, resizeMode: 'contain' }} />
                                    <Text numberOfLines={1} style={styles.textTitle}>{item.name}</Text>
                                </TouchableOpacity>
                            )}
                            itemStyle={{ height: 200, margin: 4, }}
                        />
                    </View>
                </View>
            </ScrollView>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
        backgroundColor: '#e1e7ea',
    },
    wrapper: {
        backgroundColor: '#ffffff',
    },
    containerHorizontal: {

        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
        width: '100%',
        // resizeMode: 'contain'
    },
    textCarousel: {
        color: '#ffffff',
    },
    content: {
        flex: 1,
        // alignItems: 'center',
        // justifyContent: 'center',
    },
    textTitle: {
        fontSize: 12,
        flexDirection: 'row',
    },
    imageSlide: {
        backgroundColor: 'rgba(255,255,255,0.8)',
        position: 'absolute',
        top: 130, left: 0,
        right: 0, bottom: 0,
        justifyContent: 'flex-end',
        alignItems: 'flex-start'
    },
    TextSlide: {
        color: '#262626',
        fontSize: 14,
        padding: 2
    }
});


export default connect(state => state, { push })(MoviePage)
// const mapStateToProps = (state) => {
//     return {
//         user: state.user
//     }
// }

// export default connect(
//     mapStateToProps,
// )(MoviePage)