import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList, Dimensions } from 'react-native';
import { TabBar, Icon, ActivityIndicator, Button, Tabs, Card } from '@ant-design/react-native'
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import axios from 'axios';
import seatSofa from '../images/seat3.png'
import seatSofa_check from '../images/user.png'

var options = { year: 'numeric', month: 'long', day: 'numeric' }

class BookingPage extends Component {

    state = {
        movie: [],
        movie3: [],
        seats: [],
        booking: [],
        check: true,
        total: [],
        new: [],
        loading: true
    }

    UNSAFE_componentWillMount() {
        const item = this.props.location.state.item.item
        console.log('type seattt: ', this.props.location.state.item.item._id)
        console.log('type seattt: ', this.props.location.state.item.item.startDateTime)
        // item.seats.map((i, index) => {
        //     this.setState({seatsRows: i})
        // })
        this.renderMovie()
        this.setState({ movie: this.props.location.state.item.item })
        // console.log('RRR: ', this.props.location.state.item.item)

        this.setState({ seats: item.seats })
        console.log('kkkk: ', this.props.location.state.item.item)
        console.log('kkkk2: ', this.state.movie3)

    }

    renderMovie = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${this.props.location.state.item.item._id}`)
            .then(response => {
                this.setState({
                    movie3: response.data,
                    loading: false
                })
                this.renderSeat(response.data.seats);
                console.log('kkkk22323: ', response)
            })
            .catch((error) => {
                console.error('Error POOM:', error);
            })
            .finally(() => { console.log('Finally') })
    }

    onClickBack = () => {
        const { push } = this.props
        const item = this.props.location.state.movie
        console.log('Back', item)
        push('/itemMovie', {
            item: item
        })
    }

    renderImage = () => {
        var imgSource = this.state.check ? seatSofa : seatSofa_check
        return (
            <View>
                <Image
                    source={imgSource}
                />
            </View>
        )
    }

    renderOrderSeat = (item) => {
        if (item.item.type === "SOFA") {
            return (
                <View style={styles.detailSeat}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={styles.detail}>
                                <Image style={styles.seat3} source={require('../images/seat3.png')} />
                                <Text style={styles.textDetailSeat}>{item.item.name}</Text>
                                <Text style={styles.textDetailSeat}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else if (item.item.type === "PREMIUM") {
            return (
                <View style={styles.detailSeat}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={styles.detail}>
                                <Image style={styles.seat1} source={require('../images/seat2.png')} />
                                <Text style={styles.textDetailSeat}>{item.item.name}</Text>
                                <Text style={styles.textDetailSeat}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        } else {
            return (
                <View style={styles.detailSeat}>
                    <View style={{ flexDirection: 'row', margin: 5, alignItems: 'center', justifyContent: 'center' }}>
                        <View>
                            <View style={styles.detail}>
                                <Image style={styles.seat1} source={require('../images/seat1.png')} />
                                <Text style={styles.textDetailSeat}>{item.item.name}</Text>
                                <Text style={styles.textDetailSeat}>{item.item.price} THB</Text>
                            </View>
                        </View>
                    </View>
                </View>
            )
        }
    }

    checkSeat = (item) => {
        if (item === false || this.state.seat === false) {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={styles.seat3B} source={require('../images/seat3.png')} />
            )
        } else if (item === true) {
            return (
                <Image style={styles.seat3B} source={require('../images/user.png')} />
            )
        } else if (this.state.seat === true) {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={styles.seat3B} source={require('../images/seat3.png')} />
            )
        }
        else {
            this.setState({ seat: !this.state.seat });
            return (
                <Image style={styles.seat3B} source={require('../images/user.png')} />
            )
        }
    }

    onPressSeat = (type, row, column, price, items) => {
        // this.state.booking.push({ column: (column + 1), row: (row + 1), type: type })

        let item = this.state.new
        item = item.map(i => {
            if (i.type !== type) {
                return i
            }
            return Object.keys(i).reduce((sum, each, indexRows) => {
                sum[each] = i[each]
                if (each.includes('row') && !each.includes('rows')) {
                    sum[each] = sum[each].map((item, indexCulumn) => {
                        if (each === `row${(row + 1)}` && indexCulumn === column && item === 'F') {
                            this.state.total.push(price)
                            return 'C'
                        }
                        else {
                            if (each === `row${(row + 1)}` && indexCulumn === column && item === 'C') {
                                return 'F'
                            } else {
                                return item
                            }
                        }
                    })
                    // console.log('sumEach: ', sum[each], each)
                }
                //(indexCulumn === column && indexRows === row) ? 'C': item
                return sum
            }, {})
        })
        console.log('POOM2: ', item);
        this.setState({ new: item })
        console.log('biw', items)
        if (items === 'F') {
            this.setState({
                booking: [...this.state.booking, {
                    type: type,
                    row: (row + 1),
                    column: (column + 1)
                }]
            })
        } else if (items === 'C') {
            let result = this.state.booking.filter((item) => {
                return !(item.row === row && item.column === column && item.type === type)
            })
            console.log('resultbookingChack:', result);
            this.setState({ booking: result })
        }
        // this.setState({
        //     booking: [...this.state.booking, {
        //         type: type,
        //         row: (row + 1),
        //         column: (column + 1)
        //     }]
        // })
        console.log('Booking,:', this.state.booking)
    }

    navigateSummary = () => {
        // const item = this.props.location.state.item.item
        this.props.history.push('/summaryBook', { id: this.props.location.state.item.item, 
            movie: this.props.location.state.item, 
            seats: this.state.booking, 
            item: this.state.movie,
            back: this.props.location.state.movie
        })
    }

    renderSeat = (item) => {
        // let item = this.props.location.state.item.item.seats
        item = item.map(i => {
            return Object.keys(i).reduce((sum, each) => {
                sum[each] = i[each]
                if (each.includes('row') && !each.includes('rows')) {
                    sum[each] = sum[each].map(item => item === true ? 'B' : 'F')
                    console.log();
                }
                return sum
            }, {})
        })
        console.log('POOM2: ', item);
        this.setState({ new: item })
    }

    Booking = () => {
        const { user } = this.props
        axios({
            method: 'post',
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            headers: { 'Authorization': `bearer ${user.token}` },
            data: {
                showtimeId: this.state.movie._id,
                seats: this.state.booking
            }
        })
            .then(() => {
                alert('Booking is Success')
            })
            .catch((error) => {
                console.log('MMMMMMM', error.response);
                alert(error.response)
            })
    }

    _keyExtractorSeats = (item, index) => item._id

    _keyExtractorDetailSeat = (item, index) => item._id

    render() {
        console.log('kkkk: ', this.props.location.state.item.item)
        console.log('kkkk2: ', this.state.movie3)
        console.log("SUSk", this.state.movie)
        const item = this.props.location.state.item.item
        console.log('itemM : ', item)
        const movie = this.props.location.state.item.item.movie
        var date = new Date(this.state.movie3.startDateTime)
        timeStart = date.toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        var date2 = new Date(this.state.movie3.endDateTime)
        timeEnd = date2.toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
        date = date.toLocaleDateString("th-TH", options)
        var cinema = this.props.location.state.item.item.cinema.name
        console.log('Test222: ', this.state.new)
        console.log('Booking,:', this.state.booking)
        return (
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <TouchableOpacity
                        style={styles.back}
                        onPress={this.onClickBack}
                    >
                        <Icon name="left" size="md" color="black" />
                    </TouchableOpacity>

                    <View style={styles.icon}>
                        <Text style={{ color: 'black' }}>{movie.name}</Text>
                    </View>
                    <View style={styles.back}>
                    </View>
                </View>
                <View style={styles.HeaderContent}>
                    <View style={styles.detailMovie}>
                        <Image
                            source={{ uri: movie.image }}
                            style={{ width: '20%', height: 100, resizeMode: 'contain' }}
                        />
                        <View style={styles.infoMovie}>
                            <Text style={{ color: '#0099ff', margin: 5 }}>{date}</Text>
                            <Text style={{ color: 'white', margin: 5 }}><Icon name="clock-circle" size="xs" /> {timeStart} - {timeEnd}</Text>
                            <Text style={{ color: '#0d0d0d', margin: 5 }}>{cinema} | &nbsp;<Icon name="sound" size="xxs" color="#333333" /> {item.soundtrack}</Text>
                        </View>
                    </View>
                </View>
                <FlatList
                    keyExtractor={this._keyExtractorDetailSeat}
                    numColumns={item.seats.length}
                    data={item.seats}
                    renderItem={(item) => (
                        this.renderOrderSeat(item)
                    )}
                />
                <ScrollView>
                    <View style={{ alignItems: 'center', justifyContent: 'center' }}>
                        <Image style={styles.screen} source={require('../images/screen2.png')} />
                    </View>
                    <View style={styles.content}>

                        {
                            this.state.loading === false ? (
                                this.state.new.map((i, index) => {
                                    console.log('i: ', i)
                                    return Object.keys(i).map((rowsData, indexRows) => {
                                        // Object.keys(this.state.seatsRows).map((i, index) => {
                                        //     i['row'+(indexRows+1)][index]
                                        // })
                                        console.log('rowsData: ', rowsData)
                                        // console.log('Rowww: ', i['row'+(index+1)])
                                        // console.log('new: ', this.state.seatsRows)
                                        return (
                                            <FlatList
                                                keyExtractor={this._keyExtractorSeats}
                                                data={i['row' + (indexRows + 1)]}
                                                numColumns={i.columns}
                                                extraData={this.state}
                                                renderItem={({ item, index }) => {
                                                    console.log('eieiei', i.type)
                                                    // console.log('t f: ', item)
                                                    // if (i.type === "SOFA" && item === false) {
                                                    //     return (
                                                    //         <TouchableOpacity onPress={() => { this.onPressSeat(i.type, (indexRows + 1), (index + 1), i.price) }}>
                                                    //             {/* {i['row' + (indexRows + 1)][index] === false ? (<Image style={styles.seat3B} source={require('../images/seat3.png')} />) :
                                                    //                 <Image style={styles.seat3B} source={require('../images/user.png')} />
                                                    //             } */}
                                                    //             <Image style={styles.seat3B} source={require('../images/seat3.png')} />
                                                    //         </TouchableOpacity>
                                                    //     )
                                                    // } else if (i.type === "PREMIUM" && item === false) {
                                                    //     return (
                                                    //         <TouchableOpacity onPress={() => { this.onPressSeat(i.type, (indexRows + 1), (index + 1), i.price) }}>
                                                    //             {/* {i['row' + (indexRows + 1)][index] === false ? (<Image style={styles.seat1B} source={require('../images/seat2.png')} />) :
                                                    //                 <Image style={styles.seat3B} source={require('../images/user.png')} />
                                                    //             } */}
                                                    //             <Image style={styles.seat1B} source={require('../images/seat2.png')} />
                                                    //         </TouchableOpacity>
                                                    //     )
                                                    // } else if (i.type === "DELUXE" && item === false) {
                                                    //     return (
                                                    //         <TouchableOpacity onPress={() => { this.onPressSeat(i.type, (indexRows + 1), (index + 1), i.price) }}>
                                                    //             {/* {i['row' + (indexRows + 1)][index] === false ? (<Image style={styles.seat1B} source={require('../images/seat1.png')} />) :
                                                    //                 <Image style={styles.seat3B} source={require('../images/user.png')} />
                                                    //             } */}
                                                    //             <Image style={styles.seat1B} source={require('../images/seat1.png')} />
                                                    //         </TouchableOpacity>
                                                    //     )
                                                    // } else {
                                                    //     if (i.type === "DELUXE" || i.type === "PREMIUM") {
                                                    //         return <Image style={styles.seat1B} source={require('../images/user.png')} />
                                                    //     } else {
                                                    //         return <Image style={styles.seat1B} source={require('../images/user.png')} />
                                                    //     }
                                                    // }
                                                    // if (item === 'B') {
                                                    //     return <Image style={styles.seat1B} source={require('../images/user.png')} />
                                                    // }
                                                    // else if (i.type === "SOFA" && item === 'F') {
                                                    //     <TouchableOpacity onPress={() => { this.onPressSeat(i.type, (indexRows + 1), (index + 1), i.price) }}>
                                                    //         <Image style={styles.seat1B} source={require('../images/seat3.png')} />
                                                    //     </TouchableOpacity>
                                                    // }
                                                    // else if (i.type === "PREMIUM" && item === 'F') {
                                                    //     <TouchableOpacity onPress={() => { this.onPressSeat(i.type, (indexRows + 1), (index + 1), i.price) }}>
                                                    //         <Image style={styles.seat1B} source={require('../images/seat2.png')} />
                                                    //     </TouchableOpacity>
                                                    // }
                                                    // else if (i.type === "DELUXE" && item === 'F') {
                                                    //     <TouchableOpacity onPress={() => { this.onPressSeat(i.type, (indexRows + 1), (index + 1), i.price) }}>
                                                    //         <Image style={styles.seat1B} source={require('../images/seat1.png')} />
                                                    //     </TouchableOpacity>
                                                    // }
                                                    // else if (item === 'C') {
                                                    //     <TouchableOpacity onPress={() => { this.onPressSeat(i.type, (indexRows + 1), (index + 1), i.price) }}>
                                                    //         <Image style={styles.seat1B} source={require('../images/user.png')} />
                                                    //     </TouchableOpacity>
                                                    // }
                                                    if (item === 'F') {
                                                        if (i.type === "SOFA") {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price, item) }}>
                                                                    <Image style={styles.seat3B} source={require('../images/seat3.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                        else if (i.type === "PREMIUM") {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price), item }}>
                                                                    <Image style={styles.seat1B} source={require('../images/seat2.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                        else if (i.type === "DELUXE") {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price, item) }}>
                                                                    <Image style={styles.seat1B} source={require('../images/seat1.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                    }
                                                    else if (item === 'C') {
                                                        if (i.type === "SOFA") {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price, item) }}>
                                                                    <Image style={styles.seat3B} source={require('../images/check2.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                        else {
                                                            return (
                                                                <TouchableOpacity onPress={() => { this.onPressSeat(i.type, indexRows, index, i.price, item) }}>
                                                                    <Image style={styles.seat1B} source={require('../images/check.png')} />
                                                                </TouchableOpacity>
                                                            )
                                                        }
                                                    }
                                                    else if (item === 'B') {
                                                        return <Image style={styles.seat1B} source={require('../images/user.png')} />
                                                    }
                                                }
                                                }
                                            />
                                        )
                                    })
                                }).reverse()
                            ) : (
                                    <ActivityIndicator color="#00ccff" size="large" style={{ alignItems: 'center', justifyContent: 'center' }} />

                                )
                        }
                    </View>

                </ScrollView>
                <View style={{ flex: 1, alignItems: 'center', justifyContent: 'center' }}>
                    {/* <View style={{ flexDirection: 'row' }}>
                            <Text style={{ color: 'black', fontSize: 14, justifyContent: 'flex-end' }}>{this.state.total.reduce((x, y) => { return x + y }, 0)} THB</Text>
                        </View> */}
                    <View style={{ width: '100%' }}>
                        <Button type='primary' onPress={() => { this.navigateSummary() }}>Booking</Button>
                    </View>
                </View>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#455a64',
    },
    boxHeader: {
        backgroundColor: '#d1dae0',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    back: {
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    HeaderContent: {
        flexDirection: 'column',
        justifyContent: 'center',
        margin: 10
    },
    detailMovie: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    detailSeat: {
        flex: 1,
        backgroundColor: '#34444b',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    infoMovie: {
        margin: 5,
        marginLeft: 10
    },
    seat1: {
        width: 15,
        height: 15
    },
    seat3: {
        width: 40,
        height: 15
    },
    seat1B: {
        width: 12,
        height: 12,
        margin: 2
    },
    seat3B: {
        width: 27,
        height: 10,
    },
    user: {
        width: 8,
        height: 8,
    },
    textDetailSeat: {
        color: 'white',
        fontSize: 8
    },
    detail: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    screen: {
        width: '95%',
        height: 50,
        borderRadius: 20,
    }
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, { push })(BookingPage)
