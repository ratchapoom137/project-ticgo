import React, { Component } from 'react'
import { Route, NativeRouter, Switch, Redirect } from 'react-router-native'
import { Provider } from 'react-redux'
import { store, history } from '../reducers/AppStore';
import LoginPage from './LoginPage'
import RegisterPage from './RegisterPage'
import MainPage from './MainPage'
import itemMoviePage from './itemMoviePage'
import EditUserPage from './EditUserPage'
import BookingPage from './BookingPage'
import HistoryPage from './History'
import summaryBookPage from './SummaryBooking'
import NEW from './new'
import { ConnectedRouter } from 'connected-react-router'

// import { PersistGate } from '../reducers/AppStore'

class Router extends Component {
    render() {
        return (
            <Provider store={store}>
                <ConnectedRouter history={history}>
                    <Switch>
                        <Route path="/login" component={LoginPage} />
                        <Route path="/register" component={RegisterPage} />
                        <Route path="/main" component={MainPage} />
                        <Route path="/history" component={HistoryPage} />
                        <Route path="/edituser" component={EditUserPage} />
                        <Route path="/itemMovie" component={itemMoviePage} />
                        <Route path="/bookingPage" component={BookingPage} />
                        <Route path="/summaryBook" component={summaryBookPage} />
                        <Route path="/new" component={NEW} />
                        <Redirect to="/login" />
                    </Switch>
                </ConnectedRouter>
            </Provider>

            /* 
            <Provider store={store}>
                <PersistGate loading={null} persister={persistor}>
                    <ConnectedRouter history={history}>
                        <Router />
                    </ConnectedRouter>
                </PersisterGate>
            </Provider>
            
            */
        )
    }
}

export default Router