import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Alert } from 'react-native';
import { Icon, InputItem, Button } from '@ant-design/react-native';
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import Logo from '../Logo'
import ButtonFB from '../conponents/ButtonFB'
import axios from 'axios';
// import FBSDK, { LoginManager } from 'react-native-fbsdk'
// import { Redirect } from 'react-router-native'


class LoginPage extends Component {

    state = {
        visible: false,
        email: '',
        password: '',
        firstname: '',
        lastname: '',
    }

    navigateToMainPage = () => {
        this.props.history.push('/main')
    }

    navigateToRegisterPage = () => {
        this.props.history.push('/register')
    }

    onChangeValue = (index, value) => this.setState({ [index]: value })
    onLogin = () => {
        if (this.state.email === '' || this.state.password === '') {
            Alert.alert('Please do not leave input fields blanked')
        }
        else {
            this.setState({ visible: true })
            axios({
                url: 'https://zenon.onthewifi.com/ticGo/users/login?email',
                method: 'post',
                data: {
                    email: this.state.email,
                    password: this.state.password,
                    // email: 'test2@gmail.com',
                    // password: '123456'
                }
            }).then(res => {
                const { data } = res
                const { user } = data
                this.props.addUser(data.user);
                this.props.history.push('/main', {
                    item: 'movies'
                })
            }).catch(e => {
                console.log("error " + e)
                alert('Email or password '+e.response.data.errors.email)
                this.setState({ visible: false })
            })
        }
    }

    render() {
        const { username, password } = this.state
        const { user, addUser } = this.props
        console.log(user)
        return (
            <View style={styles.container}>
                <Logo />
                <View style={{ width: '90%' }}>
                    <InputItem
                        style={{ color: '#ffffff' }}
                        clear
                        value={this.state.email}
                        onChange={email => {
                            this.setState({
                                email,
                            });
                        }}
                        placeholder="Email"
                    >
                        <Icon name="user" size="md" color="white" />
                    </InputItem>
                </View>
                <View style={{ width: '90%' }}>
                    <InputItem
                        style={{ color: '#ffffff' }}
                        clear
                        type="password"
                        secureTextEntry={true}
                        value={this.state.password}
                        onChange={password => {
                            this.setState({
                                password,
                            });
                        }}
                        placeholder="Password"
                    >
                        <Icon name="lock" size="md" color="white" />
                    </InputItem>
                </View>

                <View style={styles.button}>
                    {/* <Button
                        title="Login"
                        color="#34444b"
                        onPress={() => this.onLogin()}
                    /> */}
                    <Button loading={this.state.visible}
                        onPress={() => this.onLogin()}
                        activeStyle={{ backgroundColor: '#a6a6a6' }}
                        style={styles.loginButton}
                        type="primary"
                    >
                        <Text style={{ color: 'white' }}>Login</Text>
                    </Button>
                    {/* <Button type="primary" onPress={() => { this.onLogin() }} >Login</Button> */}
                </View>
                <Text style={{ color: 'gray' }}>───────────────────────────</Text>
                <ButtonFB />
                <View style={styles.signupTextCont}>
                    <Text style={styles.signupText}>Don't have an account yet?</Text>
                    <TouchableOpacity onPress={() => { this.navigateToRegisterPage() }}><Text style={styles.signupButton}> Signup</Text></TouchableOpacity>
                </View>
            </View>
        );

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#455a64',
        //#009999
    },
    signupTextCont: {
        flexGrow: 1,
        alignItems: 'center',
        justifyContent: 'flex-start',
        flexDirection: 'row'

    },
    signupText: {
        color: 'rgba(255,255,255,0.6)',
        fontSize: 16
    },
    signupButton: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: '500'
    },
    button: {
        width: 300,
        marginVertical: 10,
        paddingHorizontal: 12
    },
    inputBox: {
        width: 300,
        backgroundColor: 'rgba(255, 255,255,0.1)',
        borderRadius: 25,
        paddingHorizontal: 16,
        fontSize: 16,
        color: '#ffffff',
        marginVertical: 10
    },
    loginButton: {
        backgroundColor: '#34444b',
        borderRadius: 30,
        borderColor: '#34444b'
    }
});

const mapStateToProps = ({ user }) => ({
    username: user.username,
    password: user.password,
})

const mapDidpatchToProps = (dispatch) => {
    return {
        addUser: (user) => {
            dispatch({
                type: 'ADD_USER',
                user: user
            })
        }
    }
}

export default connect(
    mapStateToProps,
    mapDidpatchToProps
)(LoginPage)
