import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, Image, StatusBa, TouchableOpacity, Modal, TextField, FlatList, WingBlank } from 'react-native';
import { Button, Icon, InputItem, WhiteSpace, Card } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import axios from 'axios';

class HistoryBooking extends Component {

    state = {
        tickets: [],
        ticket: []
    }

    onClickBack = () => {
        this.props.history.push('/main', {
            item: 'profile'
        })
    }

    UNSAFE_componentWillMount() {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
            headers: { 'Authorization': `bearer ${user.token}` }
        })
            .then(res => {
                console.log('RES:', res.data);
                const dataTickets = res.data
                this.setState({ tickets: dataTickets })
                for (let index = 0; index < dataTickets.length; index++) {
                    console.log('Ticket INdex?:', dataTickets[index]);
                    axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${dataTickets[index].showtime}`)
                        .then(res => {
                            this.setState({ ticket: [...this.state.ticket, res.data] })
                            console.log('ticket:', this.state.ticket);
                            console.log('ticketS:', this.state.tickets);
                        })
                        .catch(error => {
                            console.log('ERROR_showtime:', error);
                        })
                }
            })
            .catch(error => {
                console.log('ERROR:', error);
            })
    }

    seatPrint = (seats) => {
        const seat = []
        for (let index = 0; index < seats.length; index++) {
            seat.push(<Text>{seats[index].type} Column: {seats[index].column} Row: {seats[index].row}</Text>)
        }
        return seat
    }
    addZero(time) {
        if (time < 10) {
            time = "0" + time;
        }
        return time;
    }

    _keyExtractor = (item, index) => item._id

    render() {
        const { user, editUser } = this.props
        console.log(user)
        return (
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <TouchableOpacity
                        style={styles.back}
                        onPress={this.onClickBack}
                    >
                        <Icon name="left" size="md" color="black" />
                    </TouchableOpacity>

                    <View style={styles.icon}>
                        <Text>Booking History</Text>
                    </View>
                    <View style={styles.back}>
                    </View>
                </View>
                {/* <View style={styles.content}> */}
                <View style={{ margin: 5 }}>
                    <FlatList
                        keyExtractor={this._keyExtractor}
                        inverted data={this.state.ticket}
                        renderItem={({ item, index }) =>
                            <Card>
                                <Card.Header
                                    title={item.movie.name}
                                    extra={item.movie.duration + ' min'}
                                />
                                <Card.Body>
                                    <View style={{ flexDirection: 'row' }}>
                                        <View style={{ margin: 5 }}>
                                            <TouchableOpacity>
                                                <Image style={{ width: 100, height: 180, resizeMode: 'contain' }}
                                                    source={{ uri: item.movie.image }} />
                                            </TouchableOpacity>
                                        </View>
                                        <View style={{ margin: 5 }}>
                                            <Text>{new Date(this.state.tickets[index].createdDateTime).toLocaleDateString()}</Text>
                                            {item.index}{this.seatPrint(this.state.tickets[index].seats)}
                                            <Text> Show time : {this.addZero(new Date(item.startDateTime).toLocaleDateString())} </Text>
                                            <Text> {this.addZero(new Date(item.startDateTime).getHours())} : {this.addZero(new Date(item.startDateTime).getMinutes())}</Text>
                                        </View>
                                    </View>
                                </Card.Body>
                                <Card.Footer
                                    content="Soundtrack / Subtitle"
                                    extra={item.movie.soundtracks + '/' + item.movie.subtitles}
                                />
                            </Card>
                        }
                    />
                </View>

                {/* </View> */}
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        // alignItems: 'center',    
        // justifyContent: 'center',
        backgroundColor: '#e1e7ea',
    },
    content: {
        flex: 1,
        margin: 20,
        padding: 10,
        backgroundColor: '#e1e7ea'
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        flexDirection: 'row'
    },
    text: {

    },
    TextContent: {
        flexDirection: 'row',
    },
    buttons: {
        margin: 15,
        backgroundColor: '#ffcc00',
        borderColor: '#ffcc00',
    },
    boxHeader: {
        backgroundColor: '#d1dae0',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    back: {
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, { push })(HistoryBooking)

// import React, { Component } from 'react';
// import { Text, View, StyleSheet, Image, TouchableOpacity, ScrollView, ActivityIndicator, ListItem, FlatList } from 'react-native';
// import { SearchBar, TabBar, Tabs, List, Flex, Grid, Button, InputItem, Card, WingBlank } from '@ant-design/react-native';
// // import { Icon } from 'react-native-elements'
// import axios from 'axios'
// import { push } from 'connected-react-router'
// import { connect } from 'react-redux'

// class profile extends Component {

//     onClickShowing = () => {
//         this.props.history.push('/listmovies')

//     }
//     onClickProfile = () => {
//         this.props.history.push('/profile')

//     }
//     onClickEdit = () => {
//         this.props.history.push('/editProfile')

//     }

//     onClickLogout = () => {
//         this.props.history.push('/signin')

//     }
//     onClickBookingHistory = () => {
//         this.props.history.push('/bookingHistory')

//     }

//     constructor(props) {
//         super(props);
//         this.state = {
//             selectedTab: 'Booking',
//             items: [],
//             isLoading: true,
//             email: '',
//             password: '',
//             firstname: '',
//             lastname: '',
//             imagePath: '',
//             historys: [],
//             tickets: [],
//             ticket: []

//         };
//     }

//     onChangeTab() {
//         this.setState({
//             selectedTab: tabName,
//         });
//     }

//     componentDidMount() {
//         this.getImage()
//     }

//     getImage = () => {
//         const { user } = this.props
//         axios.get('https://zenon.onthewifi.com/ticGo/movies/book', {
//             headers: {
//                 Authorization: `Bearer ${user.token}`
//             }
//         }).then(response => {
//             const data = response.data
//             this.setState({ tickets: data, isLoading: false })
//             for (let index = 0; index < data.length; index++) {
//                 //console.log('Ticket INdex:', data);
//                 axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/${data[index].showtime}`)
//                     .then(response => {
//                         this.setState({ ticket: [...this.state.ticket, response.data] })
//                         // console.log('ticket:', this.state.ticket);
//                         // console.log('ticketS:', this.state.tickets);

//                     })
//             }
//         })

//             .catch(err => { console.log(err) })
//             .finally(() => { console.log('Finally') })
//     }

//     showDateTime = (item) => {
//         console.log(item);


//     }


//     render() {
//         const { user } = this.props
//         console.log(user)
//         console.log(this.state.historys);




//         return (


//             <TabBar
//                 unselectedTintColor="#949494"
//                 tintColor="#f47373"
//                 barTintColor="#f5f5f5"
//             >
//                 <TabBar.Item
//                     title="Showing"
//                     // icon={<Icon type='material-community' name='youtube' />}
//                     //selected={this.state.selectedTab === 'Showing'}
//                     onPress={() => this.onClickShowing()}
//                 />


//                 <TabBar.Item
//                     // icon={<Icon type='material-community' name='heart' />}
//                     title="Faverite"
//                 //selected={this.state.selectedTab === 'Faverite'}
//                 //onPress={this.onClickFaverite}
//                 />

//                 <TabBar.Item
//                     // icon={<Icon type='material-community' name='history' />}
//                     title="Booking History"
//                     selected={this.state.selectedTab === 'Booking'}
//                     onPress={() => this.onClickBookingHistory()}
//                 >
//                     <View style={[styles.content]}>
//                         <View style={[styles.boxColumn]}>
//                             <View style={[styles.boxHeader]}>
//                                 <View style={[styles.rowHeader]}>
//                                     <Text style={[styles.textHead]}>Booking History</Text>
//                                 </View>
//                             </View>


//                             <View style={[styles.box1]}>
//                                 <ScrollView>
//                                     <FlatList
//                                         inverted data={this.state.ticket}
//                                         renderItem={(item) => (
//                                             <WingBlank size="lg">
//                                                 <Card style={{ margin: 5 }}>
//                                                     <Card.Header
//                                                         title={item.item.movie.name}
//                                                         thumbStyle={{ width: 30, height: 30, fontSize: 10 }}
//                                                         extra={() => { this.showDateTime(item) }}
//                                                     />
//                                                     <Card.Body>
//                                                         <View style={{ height: 42 }}>
//                                                             <Text style={{ marginLeft: 16 }}>Card Content</Text>
//                                                         </View>
//                                                     </Card.Body>
//                                                     <Card.Footer content="footer content" extra="footer extra content" />
//                                                 </Card>
//                                             </WingBlank>

//                                         )


//                                         }

//                                     />



//                                 </ScrollView>
//                             </View>



//                         </View>
//                     </View>


//                 </TabBar.Item>
//             </TabBar>

//         );
//     }
// }
// const mapStateToProps = (state) => {
//     return {
//         user: state.user
//     }
// }

// export default connect(mapStateToProps)(profile)

// const styles = StyleSheet.create({
//     content: {
//         flex: 1,
//         // alignItems: 'center',
//         // justifyContent: 'center'
//     },
//     img: {
//         backgroundColor: 'white',
//         borderRadius: 100,
//         width: 200,
//         height: 200,
//     },

//     center: {
//         alignItems: 'center',
//         justifyContent: 'center'
//     },
//     button: {
//         backgroundColor: '#f47373',
//         borderColor: '#f47373',
//         margin: 10
//     },
//     textHead: {
//         textAlign: 'center',
//         fontSize: 15,
//         color: 'black',
//     },
//     boxHeader: {
//         flexDirection: 'row',
//         backgroundColor: '#f5f5f5',
//         borderBottomWidth: 3,
//         borderBottomColor: '#f47373',
//         justifyContent: 'center',
//         padding: 15,


//     },
//     boxContent: {
//         //flexDirection: 'row',
//         backgroundColor: 'white',
//         justifyContent: 'center',
//         margin: 10

//     },
//     rowHeader: {
//         flex: 1,
//     },
//     boxColumn: {
//         flex: 1,
//         backgroundColor: '#f5f5f5',
//         flexDirection: 'column'
//     },
//     box1: {
//         flex: 1,
//         backgroundColor: 'pink'

//     },
//     box2: {
//         flex: 1,
//     },
//     textStyle: {
//         fontSize: 20
//     }
// });
