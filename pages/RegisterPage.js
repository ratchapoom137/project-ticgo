import React, { Component } from 'react';
import { StyleSheet, Text, View, TouchableOpacity, TextInput, Button, Alert, Image } from 'react-native';
import { InputItem, WhiteSpace } from '@ant-design/react-native';
import { connect } from 'react-redux'
import axios from 'axios';
export default class RegisterPage extends Component {

    state = {
        email: '',
        password: '',
        confirmPassword: '',
        firstname: '',
        lastname: '',
    }

    onRegister = async () => {
        if (this.state.email === '' || this.state.password === '' || this.state.confirmPassword === '' || this.state.firstname === '' || this.state.lastname === '') {
            Alert.alert('Please do not leave input fields blanked')
        } else {
            if (this.state.password !== this.state.confirmPassword) {
                await Alert.alert('Incorrect Input', 'Confirmed password is not matched.')
            } else {
                try {
                    await axios.post('https://zenon.onthewifi.com/ticGo/users/register?email', {
                        email: this.state.email,
                        password: this.state.password,
                        firstName: this.state.firstname,
                        lastName: this.state.lastname
                    })
                    await Alert.alert('Register successful')
                    await this.props.history.push('/login')
                } catch (error) {
                    console.log('signup res', error.response);
                }
            }
        }
    }

    // axios({
    //     url: 'https://zenon.onthewifi.com/ticGo/users/register?email',
    //     method: 'post',
    //     data: {
    //         email: this.state.email,
    //         password: this.state.password,
    //         firstName: this.state.firstname,
    //         lastName: this.state.lastname
    //     }
    // }).then(res => {
    //     const { data } = res
    //     const { user } = data
    //     this.props.history.push('/login')
    // }).catch(e => {
    //     console.log("error ", e.response)
    // })

    loginPage = () => {
        this.props.history.push('/login')
    }

    render() {
        // const { user } = this.props
        // console.log(user)
        return (
            <View style={styles.container}>
                <View style={styles.content}>
                    <Text style={styles.textTitle}>Register</Text>
                    <Text>
                        {this.state.email} - {this.state.password} - {this.state.firstname} - {this.state.lastname}
                    </Text>
                    <InputItem
                        style={styles.inputBox}
                        clear
                        value={this.state.email}
                        // onChange={email => {
                        //     this.setState({
                        //         email,
                        //     });
                        // }}
                        onChange={value => { this.setState({ email: value }) }}
                        placeholder="Email"
                    >
                    </InputItem>
                    <InputItem
                        style={styles.inputBox}
                        clear
                        type="password"
                        secureTextEntry={true}
                        value={this.state.password}
                        // onChange={password => {
                        //     this.setState({
                        //         password,
                        //     });
                        // }}
                        onChange={value => { this.setState({ password: value }) }}
                        placeholder="Password"
                    >
                    </InputItem>
                    <InputItem
                        style={styles.inputBox}
                        clear
                        type="password"
                        secureTextEntry={true}
                        value={this.state.confirmPassword}
                        // onChange={confirmPassword => {
                        //     this.setState({
                        //         confirmPassword,
                        //     });
                        // }}
                        onChange={value => { this.setState({ confirmPassword: value }) }}
                        placeholder="Confirm Password"
                    >
                    </InputItem>
                    <InputItem
                        style={styles.inputBox}
                        clear
                        value={this.state.firstname}
                        // onChange={firstname => {
                        //     this.setState({
                        //         firstname,
                        //     });
                        // }}
                        onChange={value => { this.setState({ firstname: value }) }}
                        placeholder="First name"
                    >
                    </InputItem>
                    <InputItem
                        style={styles.inputBox}
                        clear
                        value={this.state.lastname}
                        // onChange={lastname => {
                        //     this.setState({
                        //         lastname,
                        //     });
                        // }}
                        onChange={value => { this.setState({ lastname: value }) }}
                        placeholder="Last name"
                    >
                    </InputItem>
                </View>
                <View style={styles.button}>
                    <Button
                        title="Register"
                        color="#34444b"
                        onPress={() => this.onRegister()}
                    />
                </View>
                <WhiteSpace />
                <WhiteSpace />
                <WhiteSpace />
                <Text style={{ color: 'gray' }}>───────────────────────────</Text>
                <View style={styles.signupTextCont}>
                    <Text style={styles.signupText}>If you have an account!</Text>
                    <TouchableOpacity onPress={() => { this.loginPage() }}><Text style={styles.signupButton}> Signin</Text></TouchableOpacity>
                </View>
            </View>
        );

    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        justifyContent: 'center',
        alignItems: 'center',
        backgroundColor: '#455a64',
    },

    signupTextCont: {
        flex: 1,
        flexDirection: 'row'

    },

    signupText: {
        color: 'rgba(255,255,255,0.6)',
        fontSize: 16
    },

    signupButton: {
        color: '#ffffff',
        fontSize: 16,
        fontWeight: '500'
    },
    content: {
        flexGrow: 1,
        justifyContent: 'center',
        alignItems: 'center',
    },
    inputBox: {
        color: '#ffffff',
        // backgroundColor: 'rgba(255,255,255,0.6)',
    },
    textTitle: {
        color: '#ffffff',
        fontSize: 24
    },
    button: {
        alignItems: 'center',
        justifyContent: 'center'
    },
});