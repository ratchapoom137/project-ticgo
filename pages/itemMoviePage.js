import React, { Component } from 'react';
import { StyleSheet, Text, View, Image, ImageBackground, TouchableOpacity, ScrollView, FlatList, Dimensions } from 'react-native';
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { TabBar, Icon, ActivityIndicator, Button, Tabs, Card } from '@ant-design/react-native'
import axios from 'axios';
//{this.props.location.state.Username}
var options = { weekday: 'short', month: 'short', day: 'numeric' }
var tab1 = Date.now()
var tab2 = new Date(new Date().getTime() + 24 * 60 * 60 * 1000)
tab2 = tab2.getTime()
var tab3 = new Date(new Date().getTime() + 48 * 60 * 60 * 1000)
tab3 = tab3.getTime()

class itemMoviePage extends Component {

    state = {
        movie: [],
        movie2: [],
        movie3: [],
        isLoading: true,
        dates: new Date(),
        tabs: [
            { title: `${new Date().toLocaleDateString("th-TH", options)}` },
            { title: `${new Date(new Date().getTime() + 24 * 60 * 60 * 1000).toLocaleDateString("th-TH", options)}` },
            { title: `${new Date(new Date().getTime() + 48 * 60 * 60 * 1000).toLocaleDateString("th-TH", options)}` },
        ],
    }

    componentDidMount() {
        this.getMovieByTab1()
        this.getMovieByTab2()
        this.getMovieByTab3()
        console.log('Kuy', this.props.location.state.item)
    }

    getMovieByTab1 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`,
            { params: { date: tab1 } }
        )
            .then(response => {
                this.setState({
                    movie1: response.data,
                    isLoading: false
                })
                console.log("PPOOORRR", tab1)
            })
            .catch((error) => {
                console.error('Error POOM:', error);
            })
            .finally(() => { console.log('Finally') })
    }

    getMovieByTab2 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`,
            { params: { date: tab2 } }
        )
            .then(response => {
                this.setState({
                    movie2: response.data,
                    isLoading: false
                })
            })
            .catch((error) => {
                console.error('Error POOM:', error);
            })
            .finally(() => { console.log('Finally') })
    }

    getMovieByTab3 = () => {
        axios.get(`https://zenon.onthewifi.com/ticGo/movies/showtimes/movie/${this.props.location.state.item._id}`,
            { params: { date: tab3 } }
        )
            .then(response => {
                this.setState({
                    movie3: response.data,
                    isLoading: false
                })
            })
            .catch((error) => {
                console.error('Error POOM:', error);
            })
            .finally(() => { console.log('Finally') })
    }

    onClickBack = () => {
        const { push } = this.props
        push('/main', {
            item: 'movies'
        })
    }

    navigateToBookingPage = (item) => {
        const { push } = this.props
        const movie = this.props.location.state.item
        console.log("BBBB: ", item)
        push('/bookingPage', {
            item: item, movie: movie
        })
    }

    render() {
        const movie = this.props.location.state.item
        var tabsValue = [
            {
                title: this.state.tabs[0].title
            },
            {
                title: this.state.tabs[1].title
            },
            {
                title: this.state.tabs[2].title
            },
        ];

        return (
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <TouchableOpacity
                        style={styles.back}
                        onPress={this.onClickBack}
                    >
                        <Icon name="left" size="md" color="black" />
                    </TouchableOpacity>

                    <View style={styles.icon}>
                        <Image style={{ width: 90, height: 90 }}
                            source={require('../images/ticgo.png')} />
                    </View>
                    <View style={styles.back}>

                    </View>

                </View>
                <ScrollView style={{ flex: 1, }}>
                    {!this.state.isLoading ? (
                        <View style={styles.content}>
                            {/* <View style={styles.header}> */}
                            <ImageBackground
                                blurRadius={5}
                                source={{ uri: movie.image }}
                                style={styles.header}
                            >
                                <View style={[styles.headerImage]}>
                                    <Image
                                        source={{ uri: movie.image }}
                                        style={{ width: '100%', height: 180, resizeMode: 'contain' }}
                                    />
                                </View>
                                <View style={[styles.headerImage, styles.center]}>
                                    <Text style={styles.textHead}>
                                        {movie.name}
                                    </Text>
                                    <Text style={styles.textHead}>
                                        <Icon name="clock-circle" size="md" color="#0099ff" /> {movie.duration} min
                                        </Text>
                                </View>
                            </ImageBackground>
                            <View style={styles.tabstest}>
                                <Tabs tabs={tabsValue} tabBarActiveTextColor="#008ae6">
                                    <View>
                                        <Card>
                                            <Card.Header
                                                title="Digital Cinema"
                                            />
                                            <Card.Body>
                                                <FlatList
                                                    numColumns={4}
                                                    data={this.state.movie1}
                                                    renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
                                                        <View style={styles.ButtonTime}>
                                                            <Text style={{ color: '#99ccff', fontSize: 12 }}><Icon name="sound" size="xxs" color="#99ccff" /> {item.item.soundtrack}</Text>
                                                            {item.item.startDateTime > new Date() ?
                                                                <Button onPress={() => this.navigateToBookingPage(item)} activeStyle={{ backgroundColor: '#80dfff' }}>
                                                                    <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                                                </Button> :
                                                                <Button disabled activeStyle={{ backgroundColor: '#80dfff' }}>
                                                                    <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                                                </Button>}
                                                        </View>
                                                    )}
                                                />
                                            </Card.Body>
                                        </Card>
                                    </View>
                                    <View>
                                        <Card>
                                            <Card.Header
                                                title="Digital Cinema"
                                            />
                                            <Card.Body>
                                                <FlatList
                                                    numColumns={4}
                                                    data={this.state.movie2}
                                                    renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
                                                        <View style={styles.ButtonTime}>
                                                            <Text style={{ color: '#99ccff', fontSize: 12 }}><Icon name="sound" size="xxs" color="#99ccff" /> {item.item.soundtrack}</Text>
                                                            {item.item.startDateTime > new Date() ?
                                                                <Button onPress={() => this.navigateToBookingPage(item)} activeStyle={{ backgroundColor: '#80dfff' }}>
                                                                    <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                                                </Button> :
                                                                <Button disabled activeStyle={{ backgroundColor: '#80dfff' }}>
                                                                    <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                                                </Button>
                                                            }
                                                        </View>
                                                    )}
                                                />
                                            </Card.Body>
                                        </Card>
                                    </View>
                                    <View>
                                        <Card>
                                            <Card.Header
                                                title="Digital Cinema"
                                            />
                                            <Card.Body>
                                                <FlatList
                                                    numColumns={4}
                                                    data={this.state.movie3}
                                                    renderItem={(item) => ( //toLocaleTimeString().replace(/(.*)\D\d+/, '$1')
                                                        <View style={styles.ButtonTime}>
                                                            <Text style={{ color: '#99ccff', fontSize: 12 }}><Icon name="sound" size="xxs" color="#99ccff" /> {item.item.soundtrack}</Text>
                                                            {item.item.startDateTime > new Date() ?
                                                                <Button onPress={() => this.navigateToBookingPage(item)} activeStyle={{ backgroundColor: '#80dfff' }}>
                                                                    <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                                                </Button> :
                                                                <Button disabled activeStyle={{ backgroundColor: '#80dfff' }}>
                                                                    <Text style={{ color: 'black' }}>{new Date(item.item.startDateTime).toLocaleTimeString().replace(/(.*)\D\d+/, '$1')}</Text>
                                                                </Button>
                                                            }

                                                        </View>
                                                    )}
                                                />
                                            </Card.Body>
                                        </Card>
                                    </View>
                                </Tabs>
                            </View>


                        </View>

                    ) : (
                            <ActivityIndicator color="#00ccff" size="large" style={{ alignItems: 'center' }} />
                        )}
                </ScrollView>

            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#455a64',
    },
    boxHeader: {
        backgroundColor: '#d1dae0',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    content: {
        flexDirection: 'column',
        backgroundColor: '#ffcccc',
        justifyContent: 'center',
        margin: 10
    },
    scene: {
        flex: 1,
    },
    header: {
        flex: 1,
        flexDirection: 'row',
        height: 200,
        width: '100%',
    },
    headerImage: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    back: {
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start'
    },
    center: {
        alignItems: 'center'
    },
    textHead: {
        textAlign: 'center',
        fontSize: 15,
        color: 'white',
    },
    tabs: {
        backgroundColor: "white",
        flexDirection: "row",
        alignItems: "center",
        justifyContent: "center",
    },
    time: {
        alignItems: 'center',
        justifyContent: 'center',
        height: 150,
        backgroundColor: '#fff',
    },
    Touch: {
        backgroundColor: "gray",
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    textTab: {
        color: '#a6a6a6'
    },
    tabstest: {
        flex: 2,
        flexDirection: 'row',
        height: 345,
        backgroundColor: '#e1e7ea'
    },
    ButtonTime: {
        margin: 2,
        alignItems: 'center',
        justifyContent: 'center'
    }

});

export default connect(state => state, { push })(itemMoviePage)
