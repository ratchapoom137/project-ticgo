import React, { Component } from 'react';
import { Image, StyleSheet, Text, View, Alert, TextInput, TouchableOpacity, FlatList } from 'react-native';
import { connect } from 'react-redux'
import { push } from 'connected-react-router'
import { Button, Card, Icon } from '@ant-design/react-native';
import axios from 'axios'
var totalprice = 0
class summary extends Component {
    state = {
        movie: [],
        seats: [],
        total: 0
    }
    UNSAFE_componentWillMount() {
        this.setState({ movie: this.props.location.state.movie, seats: this.props.location.state.seats })
        console.log('WWWWW', this.props.location.state.movie)
        console.log('RRRRR', this.state.seats)
    }
    goToMenu = () => {
        const { user } = this.props
        // console.log('ID', this.state.movie._id)
        console.log('user', user.token)
        axios({
            method: 'post',
            url: 'https://zenon.onthewifi.com/ticGo/movies/book',
            headers: { 'Authorization': `bearer ${user.token}` },
            data: {
                showtimeId: this.props.location.state.id._id,
                seats: this.state.seats
            }
        })
            .then(() => {
                alert('Booking is Success')
                const { push } = this.props
                push('/bookingPage', {
                    item: this.props.location.state.movie, movie: this.props.location.state.item
                })
                totalprice = 0
            })
            .catch((error) => {
                console.log(error);
                alert(error)
            })
    }

    price = (item) => {
        if (item === 'SOFA') {
            totalprice = totalprice + 500
            this.setState({ total: this.state.total + 500 })
            return 500
        } else if (item === 'PREMIUM') {
            totalprice = totalprice + 190
            this.setState({ total: this.state.total + 190 })
            return 190
        } else {
            totalprice = totalprice + 150
            this.setState({ total: this.state.total + 150 })
            return 150
        }
    }

    onClickBack = () => {
        const { push } = this.props
        const item = this.props.location.state.movie
        push('/bookingPage', {
            item: this.props.location.state.movie, movie: this.props.location.state.back
        })
    }

    render() {
        const { user } = this.props
        console.log('showTime Movie:', this.state.movie);
        console.log('seats:', this.state.seats);
        console.log('token:', user.token);
        console.log('movie id :', this.state.movie.item.movie.image);

        return (
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <TouchableOpacity
                        style={styles.back}
                        onPress={this.onClickBack}
                    >
                        <Icon name="left" size="md" color="black" />
                    </TouchableOpacity>
                    <View style={styles.icon}>
                        <Text style={{ color: 'black' }}>Summary price Booking</Text>
                    </View>
                    <View style={styles.back}>
                    </View>
                </View>
                <View style={{ flex: 1, alignItems: 'center' }}>
                    <FlatList
                        data={this.state.seats}
                        renderItem={({ item }) =>
                            <Card style={{ width: 300 }}>
                                <Card.Header
                                    title='Ticket'
                                />
                                <Card.Body>
                                    <View style={{ flexDirection: 'row' }}>
                                        <Image
                                            source={{ uri: this.state.movie.item.movie.image }}
                                            style={{ width: 70, height: 70, resizeMode: 'contain' }}
                                        />
                                        <View>
                                            <Text style={{ fontSize: 13 }}> SeatType:{item.type} </Text>
                                            <Text style={{ fontSize: 15 }}> R:{item.row} C:{item.column} </Text>
                                            <Text style={{ fontSize: 15 }}> Price: {this.price(item.type)} THB </Text>
                                        </View>
                                        <View style={{ alignItems: 'center' }}>

                                        </View>
                                    </View>
                                </Card.Body>
                            </Card>
                        } />
                </View>
                <View style={{backgroundColor: '#2d5986', alignItems: 'center', justifyContent: 'center'}}>
                    <Text style={{ fontSize: 28, color: '#ffff00'}}>Total : {totalprice} THB</Text>
                    <Button style={{width:'100%'}} type='primary' onPress={() => { this.goToMenu() }}>Confirm</Button>
                </View>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#455a64',
    },
    boxHeader: {
        backgroundColor: '#d1dae0',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    back: {
        padding: 10,
        margin: 2,
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center'
    },
    icon: {
        flex: 5,
        alignItems: 'center',
        justifyContent: 'flex-start',
    },
    HeaderContent: {
        flexDirection: 'column',
        justifyContent: 'center',
        margin: 10
    },
    detailMovie: {
        flexDirection: 'row',
        alignItems: 'flex-start',
    },
    detailSeat: {
        flex: 1,
        backgroundColor: '#34444b',
        alignItems: 'center',
        justifyContent: 'center',
        flexDirection: 'row',
    },
    content: {
        flex: 1,
        margin: 20,
        alignItems: 'center',
        justifyContent: 'center',
    },
    infoMovie: {
        margin: 5,
        marginLeft: 10
    },
    seat1: {
        width: 15,
        height: 15
    },
    seat3: {
        width: 40,
        height: 15
    },
    seat1B: {
        width: 10,
        height: 10,
        margin: 1
    },
    seat3B: {
        width: 25,
        height: 8,
    },
    user: {
        width: 8,
        height: 8,
    },
    textDetailSeat: {
        color: 'white',
        fontSize: 8
    },
    detail: {
        alignItems: 'center',
        justifyContent: 'center',
        marginLeft: 10,
        marginRight: 10
    },
    screen: {
        width: '100%',
        height: 50,
        borderRadius: 20,
    }
});


const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}
export default connect(mapStateToProps, { push })(summary)
