import React, { Component } from 'react';
import { ScrollView, StyleSheet, Text, View, Image, StatusBa, TouchableOpacity, Modal, TextField } from 'react-native';
import { Button, Icon, InputItem, WhiteSpace, ActivityIndicator } from '@ant-design/react-native'
import { push } from 'connected-react-router'
import { connect } from 'react-redux'
import ImagePicker from 'react-native-image-picker'
import axios from 'axios';

class ProfilePage extends Component {

    state = {
        email: '',
        password: '',
        firstname: '',
        lastname: '',
        imagePath: '',
        loadImage: true
    }

    componentDidMount() {
        const { user } = this.props
        axios.get('https://zenon.onthewifi.com/ticGo/users', {
            headers: {
                Authorization: `Bearer ${user.token}`
            }
        })
            .then(response => {
                this.setState({
                    imagePath: response.data.user.image,
                    loadImage: false
                })


            })
            .catch(err => { console.log(err) })
            .finally(() => { console.log('Finally') })

    }

    navigateToItemMoviePage = () => {
        const { push } = this.props
        push('/edituser')
    }

    navigateToEditUserPage = () => {
        this.props.history.push('/edituser')
    }

    logout = () => {
        // this.props.addUser({});
        const { push } = this.props
        push('/login')
    }

    editPage = () => {
        // this.props.addUser({});
        const { push } = this.props
        push('/edituser')
    }

    gotoHistory = () => {
        const { push } = this.props
        push('/history')
    }

    onPressImage = () => {
        const { user } = this.props
        ImagePicker.showImagePicker({}, (response) => {
            console.log(response)
            if (response.didCancel) {
                console.log('User cancelled image picker');
            } else if (response.error) {
                console.log('ImagePicker Error: ', response.error);
            } else if (response.customButton) {
                console.log('User tapped custom button: ', response.customButton);
            } else if (response.uri) {
                const formData = new FormData()
                formData.append('image', {
                    uri: response.uri,
                    name: response.fileName,
                    type: response.type,
                })
                axios.post('https://zenon.onthewifi.com/ticGo/users/image', formData, {
                    headers: {
                        Authorization: `Bearer ${user.token}`
                    },
                    onUploadProgress: progressEvent => {
                        console.log('progress', Math.floor(progressEvent.loaded / progressEvent.total * 100))
                    }
                })
                    .then(response => {
                        this.setState({
                            imagePath: response.data.image,
                            loadImage: false
                        })
                    })
                    .catch(error => {
                        console.log(error.response)
                    })
                // this.setState({
                //     imageURI: response.uri
                // })
            }
        })
    }

    logOut = () => {
        const { push } = this.props
        push('/login')
    }


    render() {
        const { user, editUser } = this.props
        console.log(user)
        return (
            <View style={styles.container}>
                <ScrollView>
                    <View style={styles.content}>

                        <TouchableOpacity onPress={this.onPressImage} style={[styles.img]}>
                            <View style={{ position: 'absolute', left: 15, top: 10, backgroundColor: 'white', borderRadius: 100, }}>
                                <Icon name="camera" size="lg" color="#455a64" />
                            </View>
                            {this.state.loadImage === false ? (
                                <Image
                                    style={{ width: '100%', height: '100%', borderRadius: 100, position: 'relative' }}
                                    source={{ uri: this.state.imagePath }}
                                />
                            ) : (
                                    <ActivityIndicator color="#00ccff" size="large" style={{ alignItems: 'center', justifyContent: 'center' }} />

                                )}
                        </TouchableOpacity>
                        <View>
                            <Text style={styles.title}>Email :</Text>
                            <Text style={styles.text}>&nbsp;&nbsp;{user.email}</Text>
                        </View>
                        <View>
                            <Text style={styles.title}>First Name :</Text>
                            <Text style={styles.text}>&nbsp;&nbsp;{user.firstName}</Text>
                        </View>
                        <View>
                            <Text style={styles.title}>Last Name :</Text>
                            <Text style={styles.text}>&nbsp;&nbsp;{user.lastName}</Text>
                        </View>
                        <Button
                            style={styles.buttons}
                            type="warning"
                            onPress={() => this.editPage()}
                        // onPress={() => this.setModalVisible(true)}
                        >
                            <Icon name="edit" size="sm" color="white" /> Edit
                        </Button>
                        <View>
                            <Button
                                style={{ backgroundColor: '#006699', borderColor: '#006699'}}
                                type="primary"
                                onPress={() => this.gotoHistory()}
                            >
                                <Icon name="book" size="sm" color="white" />History booking
                        </Button>
                        </View>
                        <Button
                            style={styles.buttons}
                            type="primary"
                            onPress={() => this.logOut()}
                        // onPress={() => this.setModalVisible(true)}
                        >
                            <Icon name="logout" size="sm" color="white" /> Logout
                    </Button>
                    </View>
                </ScrollView>
            </View>
        )
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        alignItems: 'center',
        justifyContent: 'center',
        backgroundColor: '#e1e7ea',
    },
    content: {
        flex: 1,
        margin: 20,
        padding: 10,
    },
    title: {
        fontWeight: 'bold',
        fontSize: 20,
        flexDirection: 'row'
    },
    text: {

    },
    TextContent: {
        flexDirection: 'row',
    },
    buttons: {
        margin: 15
    },
    img: {
        backgroundColor: 'white',
        borderRadius: 100,
        width: 200,
        height: 200,
        alignItems: 'center',
        justifyContent: 'center',
    },
});

const mapStateToProps = (state) => {
    return {
        user: state.user
    }
}

export default connect(mapStateToProps, { push })(ProfilePage)