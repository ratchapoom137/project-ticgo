import React, { Component } from 'react';
import { Platform, StyleSheet, Text, View, Image, TouchableOpacity } from 'react-native';
import { TabBar, Icon, Grid } from '@ant-design/react-native'
import MoviePage from './MoviePage'
import ProfilePage from './ProfilePage'
import axios from 'axios';

export default class App extends Component {

    state = {
        selectedTab: this.props.location.state.item,
        itemMovies: [],
        pathName: '',
        email: '',
        imageUrl: '',
    }

    componentDidMount() {

    }

    onChangeTab = (tabName) => {
        this.setState({
            selectedTab: tabName,
        });
        // this.props.history.push(tabName)
    }

    onClickMovie = () => {
        this.props.history.push('/itemMovie')
    }

    render() {
        // console.log(this.state.itemMovies[1].name)
        return (
            <View style={styles.container}>
                <View style={styles.boxHeader}>
                    <Image style={{ width: 90, height: 90 }}
                        source={require('../images/ticgo.png')} />
                </View>
                <TabBar
                    unselectedTintColor="#a6a6a6"
                    tintColor="#33A3F4"
                    barTintColor="#455a64"
                >
                    <TabBar.Item
                        title="Movies"
                        icon={<Icon name="video-camera" />}
                        selected={this.state.selectedTab === 'movies'}
                        onPress={() => this.onChangeTab('movies')}
                    >
                        <MoviePage props={this.props} />
                    </TabBar.Item>

                    <TabBar.Item
                        icon={<Icon name="profile" />}
                        title="Profile"
                        // badge={2}
                        selected={this.state.selectedTab === 'profile'}
                        onPress={() => this.onChangeTab('profile')}
                    >
                        <ProfilePage />
                    </TabBar.Item>

                </TabBar>
            </View>
        );
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        backgroundColor: '#e1e7ea',
    },
    welcome: {
        fontSize: 20,
        textAlign: 'center',
        margin: 10,
    },
    instructions: {
        textAlign: 'center',
        color: '#333333',
        marginBottom: 5,
    },
    boxHeader: {
        backgroundColor: 'white',
        height: 50,
        alignItems: 'center',
        justifyContent: 'center',
    }
});
